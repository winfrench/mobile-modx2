ModX Mobile extension
William French
Version 0.01 Alpha
-----------------------------------------------

The purpose of this extension is to create a Mobile extension for ModX, based on the ModMobile extension.  Since the pre-existing extension has various issues with 
the latest versions of ModX, we intend to improve upon this, as well as, add more functionality and simplicity to the extension. 